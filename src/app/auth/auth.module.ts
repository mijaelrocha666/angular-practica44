import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspecialidadesComponent } from './pages/especialidades/especialidades.component';
import { CarrerasComponent } from './pages/carreras/carreras.component';
import { MarcaslaptopComponent } from './pages/marcaslaptop/marcaslaptop.component';
import { MarcasgaseosaComponent } from './pages/marcasgaseosa/marcasgaseosa.component';
import { AuthRoutingModule } from './auth-routing.module';



@NgModule({
  declarations: [
    EspecialidadesComponent,
    CarrerasComponent,
    MarcaslaptopComponent,
    MarcasgaseosaComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule
  ]
})
export class AuthModule { }
