import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcasgaseosaComponent } from './marcasgaseosa.component';

describe('MarcasgaseosaComponent', () => {
  let component: MarcasgaseosaComponent;
  let fixture: ComponentFixture<MarcasgaseosaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarcasgaseosaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcasgaseosaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
