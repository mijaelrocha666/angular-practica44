import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcaslaptopComponent } from './marcaslaptop.component';

describe('MarcaslaptopComponent', () => {
  let component: MarcaslaptopComponent;
  let fixture: ComponentFixture<MarcaslaptopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarcaslaptopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcaslaptopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
