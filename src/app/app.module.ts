import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EspecialidadesComponent } from './auth/pages/especialidades/especialidades.component';
import { CarrerasComponent } from './auth/pages/carreras/carreras.component';
import { MarcaslaptopComponent } from './auth/pages/marcaslaptop/marcaslaptop.component';
import { MarcasgaseosaComponent } from './auth/pages/marcasgaseosa/marcasgaseosa.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
